# Ansible role: Influxdb

[![Version](https://img.shields.io/badge/latest_version-2.0.0-green.svg)](https://code.waks.be/nishiki/ansible-role-influxdb/releases)
[![License](https://img.shields.io/badge/license-Apache--2.0-blue.svg)](https://code.waks.be/nishiki/ansible-role-influxdb/src/branch/main/LICENSE)
[![Build](https://code.waks.be/nishiki/ansible-role-influxdb/actions/workflows/molecule.yml/badge.svg?branch=main)](https://code.waks.be/nishiki/ansible-role-influxdb/actions?workflow=molecule.yml)

Install and configure InfluxDB 2

## Requirements

- Ansible >= 2.10
- Debian
  - Bookworm

## Role variables

- `influxdb_init_username` - user created during the influxdb setup
- `influxdb_init_org` - organization created during the influxdb setup
- `influxdb_init_bucket` - bucket created during the influxdb setup
- `influxdb_api_token` - token to manage influxdb
- `influxdb_orgs` - hash with organizations

  ```yaml
  myorg:
    description: it's a test
    state: present
  ```

- `influxdb_buckets` - hash with the buckets

  ```yaml
  mybucket:
    description: KFC
    retention: 3600
    org: neworg
    state: present
  ```

- `influxdb_users` - hash with the users

  ```yaml
  myuser:
    status: active
    state: present
  ```

- `influxdb_authorizations` - array with the authorizations

  ```yaml
  - user: myuser
    description: write bucket
    org: neworg
    status: active
    state: present
    permissions:
      - action: write
        resource:
          type: buckets
          org: myorg
          name: mybucket
      - action: read
        resource:
          type: buckets
  ```

## How to use

```
- hosts: server
  roles:
    - influxdb
```

## Development

### Test with molecule and docker

- install [docker](https://docs.docker.com/engine/installation/)
- install `python3` and `python3-pip`
- install molecule and dependencies `pip3 install molecule molecule-plugins[docker] ansible-lint pytest-testinfra yamllint`
- run `molecule test`

## License

```
Copyright (c) 2019 Adrien Waksberg

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
```
