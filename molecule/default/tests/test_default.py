import testinfra.utils.ansible_runner

CURL = ('curl --header "Authorization: Token SuP3rS3cr3t"'
        ' http://localhost:8086/api/v2')


def test_packages(host):
    package = host.package('influxdb2')
    assert package.is_installed


def test_service(host):
    service = host.service('influxdb')
    assert service.is_running
    assert service.is_enabled


def test_sockets(host):
    socket = host.socket('tcp://0.0.0.0:8086')
    assert socket.is_listening


def test_users(host):
    cmd = host.run('{}/users -X GET'.format(CURL))
    assert cmd.succeeded
    assert '"name": "admin"' in cmd.stdout
    assert '"name": "telegraf"' in cmd.stdout


def test_org(host):
    cmd = host.run('{}/orgs -X GET'.format(CURL))
    assert cmd.succeeded
    assert '"name": "neworg"' in cmd.stdout
    assert '"name": "init_org"' not in cmd.stdout


def test_bucket(host):
    cmd = host.run('{}/buckets -X GET'.format(CURL))
    assert cmd.succeeded
    assert '"name": "mybucket"' in cmd.stdout
    assert '"everySeconds": 3600' in cmd.stdout
    assert '"name": "test"' not in cmd.stdout


def test_auth(host):
    cmd = host.run('{}/authorizations -X GET'.format(CURL))
    assert cmd.succeeded
    assert '"description": "write telegraf"' in cmd.stdout
