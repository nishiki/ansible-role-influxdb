# CHANGELOG

This project adheres to [Semantic Versioning](http://semver.org/).
Which is based on [Keep A Changelog](http://keepachangelog.com/)

## [Unreleased]

### Break

- upgrade to influxdb 2

### Changed

- test: use personal docker registry

## [2.0.0] 2021-08-18

### Break

- change user's privileges

### Added

- add retention policies
- add support for debian 11
- add support for debian 10 and python3

### Changed

- test: replace kitchen to molecule
- chore: use FQCN for module name

### Fixed

- fix no_log

### Removed

- remove support for debian 9
- the influxdb_api_password variable isn't set by default

## [1.0.0] 2019-04-12

- first version
